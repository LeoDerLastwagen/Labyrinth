package org.example;

import org.example.labyrinth.Labyrinth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.LinkedList;

public class Main {
    private final static String CONSOLE_ERROR_MESSAGE = "An error occurred while reading from the console";

    public static void main(String[] args) {
        solveLabyrinths();
    }

    // Calculates the shortest paths for each Labyrinth (levels seperated by empty lines) in the console
    // Outputs the results when entering an empty Labyrinth ("0 0 0")
    private static void solveLabyrinths() {
        // setup
        LinkedList<String> results = new LinkedList<>();
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

        // read input from console and solve labyrinths
        boolean repeat = true;
        while (repeat) {
            try {
                Labyrinth labyrinth = Labyrinth.read(input);
                if (labyrinth.isEmpty())
                    repeat = false;
                else {
                    int minutes = labyrinth.calculateFastestWayOut();
                    String result = minutes > 0 ? "Entkommen in " + minutes + " Minute(n)!" : "Gefangen :-(";
                    results.add(result);
                }
            } catch (IOException e) {
                repeat = false;
                System.out.println();
                results.add(CONSOLE_ERROR_MESSAGE);
            } catch (InputMismatchException e) {
                repeat = false;
                System.out.println();
                results.add(e.getMessage() + " (labyrinth " + (results.size() + 1)  + ")");
            }
        }

        // output results
        for (String result : results) {
            System.out.println(result);
        }
    }
}