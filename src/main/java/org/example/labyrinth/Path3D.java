package org.example.labyrinth;

import java.util.LinkedList;

// A list of 3D points
class Path3D extends LinkedList<Point3D> {
    private Path3D() {

    }

    Path3D(Point3D start) {
        add(start);
    }

    Path3D copy() {
        Path3D copy = new Path3D();
        for (Point3D point : this) {
            copy.add(point.copy());
        }
        return copy;
    }

    Path3D copyAndAdd(Point3D additional) {
        Path3D copy = copy();
        copy.add(additional);
        return copy;
    }
}
