package org.example.labyrinth;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.LinkedList;

// A Labyrinth is a three-dimensional (char) array which contains bricks as '#' and air as '.'
// It also contains one start as 'S' and one end as 'E'
// Each level ends with an empty line
// example (10 steps from start to finish)
// 2 4 5
// S....
// .###.
// .##..
// ###.#
//
// #####
// #####
// ##.##
// ##..E
//
public class Labyrinth {
    private final char[][][] blocks; // [level][row][column]

    // Chars that describe the labyrinth
    private final static char START = 'S';
    private final static char END = 'E';
    private final static char AIR = '.';
    private final static char BRICK = '#';
    private final static LinkedList<Character> elements = new LinkedList<>() {{
        add(START);add(END);add(AIR);add(BRICK);}};

    // Chars that NOT describe the labyrinth
    private final static char MARKED = 'M'; // used by the breadth-first search

    public Labyrinth(char[][][] blocks) {
        this.blocks = blocks;
    }

    // Builds a labyrinth out of the input of a reader
    // Throws InputMismatchException if the input lines does not fit the labyrinth definition (-> def above)
    public static Labyrinth read(BufferedReader input) throws IOException {
        int numberOfLevels;
        int length;
        int width;
        int line = 1;

        // read the first line of the labyrinth (numberOfLevels length width)
        try {
            String[] firstLine = input.readLine().trim().split(" ");
            numberOfLevels = Integer.parseInt(firstLine[0]);
            length = Integer.parseInt(firstLine[1]);
            width = Integer.parseInt(firstLine[2]);
        }
        catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            throw new InputMismatchException(
                    "Expected three Integers in line 1 seperated by an empty space (numberOfLevels length width)");
        }
        if ((numberOfLevels <= 0 || length <= 0 || width <= 0) // not allowed
                && !(numberOfLevels == 0 && length == 0 && width == 0)) // excepted all are 0 (empty labyrinth) that's ok
            throw new InputMismatchException(
                    "numberOfLevels, length and width have to be greater than zero (excepted \"0 0 0\")");

        // read the rest of the lines (the labyrinth blocks)
        boolean startFound = false;
        char[][][] blocks = new char[numberOfLevels][length][width];
        for (int level = 0; level < numberOfLevels; ++level) {
            for (int row = 0; row < length; ++row) {
                ++line;
                char[] currentLine = input.readLine().trim().toCharArray();
                for (int column = 0; column < width; ++column) {
                    try {
                        if (!elements.contains(currentLine[column])) {
                            throw new InputMismatchException("Only the chars " + elements + " allowed in line "+ line);
                        }
                        startFound = foundMoreThanOneStart(startFound, currentLine[column]);
                        blocks[level][row][column] = currentLine[column];
                    }
                    catch (ArrayIndexOutOfBoundsException e) {
                        throw new InputMismatchException(
                                "Expected " + width + " chars in line " + line);
                    }
                }
            }
            ++line;
            if (!input.readLine().isBlank()) { // checks if there is an empty line at the end of a level
                throw new InputMismatchException(
                        "Expected empty line (end of a level) in line " + line);
            }
        }
        return new Labyrinth(blocks);
    }

    private static boolean foundMoreThanOneStart(boolean startFound, char currentChar) {
        if (startFound && currentChar == START)
            throw new InputMismatchException("Only one start point allowed");
        return startFound || currentChar == START;
    }

    // A labyrinth that looks like "0 0 0" is empty
    public boolean isEmpty() {
        return blocks.length <= 0;
    }

    // Finds the shortest path from start to end (breadth-first search)
    // Returns the number of points in the path (start excluded) to reach the end (included)
    // Returns -1 if no path exists
    // Throws InputMismatchException if the start point is missing
    public int calculateFastestWayOut() {
        // setup
        char[][][] blocks = this.blocks.clone();
        LinkedList<Path3D> currentPaths = new LinkedList<>();
        LinkedList<Path3D> nextPaths = new LinkedList<>();
        currentPaths.add(new Path3D(findLabyrinthStart()));
        int x;
        int y;
        int z;

        // breadth-first search
        boolean repeat = true;
        while (repeat) {
            if (currentPaths.size() == 0) // no way found (from start to end)
                repeat = false;
            for (Path3D path : currentPaths) {
                x = path.getLast().getX();
                y = path.getLast().getY();
                z = path.getLast().getZ();
                for (int i = -1; i <= 1; ++i) {
                    for (int j = -1; j <= 1; ++j) {
                        for (int k = -1; k <= 1; ++k) {
                            if (Math.abs(i) + Math.abs(j) + Math.abs(k) == 1 // only one value deviates (neighbor point)
                                    && repeat // not already found the end
                                    && x+i >= 0 && x+i < blocks.length
                                    && y+j >= 0 && y+j < blocks[x].length
                                    && z+k >= 0 && z+k < blocks[x][y].length
                                    && (blocks[x+i][y+j][z+k] == AIR || blocks[x+i][y+j][z+k] == END)) { // found...
                                // ...the next point in a path (or the end)

                                nextPaths.add(path.copyAndAdd(new Point3D(x+i,y+j,z+k)));
                                if (blocks[x+i][y+j][z+k] == END)
                                    repeat = false;
                                blocks[x+i][y+j][z+k] = MARKED;
                            }
                        }
                    }
                }
            }
            currentPaths.clear();
            currentPaths.addAll(nextPaths);
            nextPaths.clear();
        } // now paths is empty or paths.getLast() is (one of) the shortest path(s)
        return currentPaths.isEmpty() ? -1 : currentPaths.getLast().size() -1; // "size - 1" because start point does not count
    }

    // Finds labyrinth start
    // Throws InputMismatchException if the start point is missing
    private Point3D findLabyrinthStart() {
        Point3D result = null;
        int i = 0;
        int j = 0;
        int k = 0;
        boolean startNotFound = true;
        while (startNotFound && i < blocks.length) {
            j = 0;
            while (startNotFound && j < blocks[i].length) {
                k = 0;
                while (startNotFound && k < blocks[i][j].length) {
                    if (blocks[i][j][k] == START) {
                        result = new Point3D(i,j,k);
                        startNotFound = false;
                    }
                    ++k;
                }
                ++j;
            }
            ++i;
        }
        if (startNotFound)
            throw new InputMismatchException("The start point is missing");
        return result;
    }
}
