package org.example.labyrinth;

// A point with three coords (x,y,z)
class Point3D {
    private final int x;
    private final int y;
    private final int z;

    Point3D(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    int getZ() {
        return z;
    }

    Point3D copy() {
        return new Point3D(x, y, z);
    }
}
