package org.example.labyrinth;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.*;

class LabyrinthTest {

    @ParameterizedTest
    @CsvSource({"0,11", "1,-1","4,16","5,14","6,-1"})
    void calculateFastestWayOut_ValidInput_ReturnsRightValues(int labyrinthIndex, int expectedResult) {
        Labyrinth labyrinth = new Labyrinth(LABYRINTHS[labyrinthIndex]);
        assertEquals(expectedResult, labyrinth.calculateFastestWayOut());
    }

    @ParameterizedTest
    @ValueSource(ints = {2,3})
    void calculateFastestWayOut_InvalidInput_ThrowsException(int labyrinthIndex) {
        Labyrinth labyrinth = new Labyrinth(LABYRINTHS[labyrinthIndex]);
        InputMismatchException exception =
                assertThrows(InputMismatchException.class, labyrinth::calculateFastestWayOut);
        assertEquals("The start point is missing", exception.getMessage());
    }

    @ParameterizedTest
    @CsvSource({"0,false", "1,false","2,true"})
    void isEmpty(int labyrinthIndex, boolean expectedResults) {
        Labyrinth labyrinth = new Labyrinth(LABYRINTHS[labyrinthIndex]);
        assertEquals(expectedResults, labyrinth.isEmpty());
    }

    private final static char[][][] LABYRINTH_0 = // end is reachable from start in 11 steps
            {{{'S','.','.','.','.'},
              {'.','#','#','#','.'},
              {'.','#','#','.','.'},
              {'#','#','#','.','#'}},

             {{'#','#','#','#','#'},
              {'#','#','#','#','#'},
              {'#','#','.','#','#'},
              {'#','#','.','.','.'}},

             {{'#','#','#','#','#'},
              {'#','#','#','#','#'},
              {'#','.','#','#','#'},
              {'#','#','#','#','E'}}};

    private final static char[][][] LABYRINTH_1 = // end is not reachable from start
            {{{'S','#','#'},
              {'#','E','#'},
              {'#','#','#'}}};

    private final static char[][][] LABYRINTH_2 = {}; // empty

    private final static char[][][] LABYRINTH_3 = // start is missing
            {{{'.','#','#'},
              {'#','E','#'},
              {'#','#','#'}}};

    private final static char[][][] LABYRINTH_4 = // end is reachable from start in 16 steps
              {{{'S','.','.','.','.'},
                {'.','#','#','#','.'},
                {'.','#','#','.','.'},
                {'#','#','#','.','#'}},

               {{'#','#','#','#','#'},
                {'#','#','#','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','.','.'}},

               {{'#','#','#','#','#'},
                {'#','#','#','#','#'},
                {'#','.','#','#','#'},
                {'#','#','#','#','.'}},

               {{'#','#','#','#','#'},
                {'#','#','E','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','.','.'}}};

    private final static char[][][] LABYRINTH_5 = // end is reachable from start in 14 steps
              {{{'S','.','.','.','.'},
                {'.','#','#','#','.'},
                {'.','#','#','.','.'},
                {'#','#','#','.','#'}},

               {{'#','#','#','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','.','.'}},

               {{'#','#','#','#','#'},
                {'#','#','.','#','#'},
                {'#','.','#','#','#'},
                {'#','#','#','#','.'}},

               {{'#','#','#','#','#'},
                {'#','#','E','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','.','.'}}};

    private final static char[][][] LABYRINTH_6 = // end is not reachable from start
              {{{'S','.','.','.','.'},
                {'.','#','#','#','.'},
                {'.','#','#','.','.'},
                {'#','#','#','.','#'}},

               {{'#','#','#','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','.','#'}},

               {{'#','#','#','#','#'},
                {'#','#','#','#','#'},
                {'#','.','#','#','#'},
                {'#','#','#','#','.'}},

               {{'#','#','#','#','#'},
                {'#','#','E','#','#'},
                {'#','#','.','#','#'},
                {'#','#','.','.','.'}}};

    private final static char[][][][] LABYRINTHS = {LABYRINTH_0, LABYRINTH_1, LABYRINTH_2, LABYRINTH_3,
                                                    LABYRINTH_4, LABYRINTH_5, LABYRINTH_6};
}